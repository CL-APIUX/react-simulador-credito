const express = require("express"),
app = express(),
bodyParser  = require("body-parser"),
methodOverride = require("method-override");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


let clientList = require('./clients.js'); 
let router = express.Router();
const simulations = [];
const operations = [];


router.post('/simulator/simulate', function(req, res) {
    const creditData = {};
    let found = false;

    const operationId = 100000 + Math.floor((999999 - 100000) * Math.random());
    creditData.operationId = operationId;
    if(!!(req.body.rut)){
        clientList.clients.map((element) => {
            if(element.rut===req.body.rut){
                found=true;
                const client = {
                    rut : element.rut,
                    firstName: element.firstName,
                    lastName: element.lastName, 
                    email: element.email, 
                    phone: element.phone, 
                    clientType: element.clientType
                };
                creditData.client = client;
            }
        });
        if(!found){
            const client = {
                rut : req.body.rut,
                firstName: '',
                lastName: '', 
                email: '', 
                phone: '', 
                clientType: 0
            };
            creditData.client = client;
        }
    }

    if(!!(req.body.capital) && !!(req.body.periodo)){
        let taxTAE = 21.32; 
        switch (creditData.client.clientType){
            case 0:
                taxTAE = 21.32; 
                break;
            case 1:
                taxTAE = 12.34; 
                break;
            case 2:
                taxTAE = 15.78; 
                break;
            case 3:
                taxTAE = 17.45; 
                break;
            default:
                taxTAE = 21.32; 
        }
    
        const interes = taxTAE/1200;
        const factor = Math.pow(interes+1, req.body.periodo);
        const cuota = req.body.capital*interes*factor/(factor-1);

        const simulation = {
            capital : req.body.capital,
            interes : taxTAE,
            periodo : req.body.periodo,
            cuota: Math.round(cuota),
            costoTotal :  Math.round(cuota * req.body.periodo)
        }
        creditData.simulation = simulation;

        const frenchaAmortizationTable = [];

        let pendingCapital = req.body.capital;
        let payedCapital = 0;
        frenchaAmortizationTable.push({numeroCuota: 0, cuota: 0, interes: 0, amortizacion: 0, saldoInsoluto: pendingCapital, deudaExtinguida: payedCapital})
        for(let i=1;i<=req.body.periodo;i++){
            frenchaAmortizationTable.push({numeroCuota: i, cuota: Math.round(cuota), interes: Math.round(interes * pendingCapital), amortizacion: Math.round(cuota - (interes * pendingCapital)) , saldoInsoluto: Math.round(pendingCapital - (cuota - (interes * pendingCapital))), deudaExtinguida: Math.abs(Math.round(payedCapital - (cuota - (interes * pendingCapital))))})
            pendingCapital = Math.round(pendingCapital - (cuota - (interes * pendingCapital)));
            payedCapital = Math.round(payedCapital - (cuota - (interes * pendingCapital)));
        }

        creditData.frenchaAmortizationTable = frenchaAmortizationTable;

    }

    simulations.push({operationId : operationId, ...creditData});
    res.status(200).jsonp(creditData);	
});




router.get('/simulator/:simulationId', function(req, res) {
    let operation = [];
    simulations.map((data) => {
        if(data.operationId==req.params.simulationId){
            operation.push(data);
        }
    });
    res.status(200).jsonp(operation);	
});

router.get('/simulator/client/:clientId', function(req, res) {
    let clientData = {};
    if(!!(req.params.clientId)){
        clientList.clients.map((element) => {
            if(element.rut===req.params.clientId){
                found=true;
                const client = {
                    rut : element.rut,
                    firstName: element.firstName,
                    lastName: element.lastName, 
                    email: element.email, 
                    phone: element.phone, 
                };
                clientData = client;
            }
        });
        if(!found){
            const client = {
                rut : req.params.clientId,
                firstName: '',
                lastName: '', 
                email: '', 
                phone: ''
            };
            clientData = client;
        }
    }
    res.status(200).jsonp(clientData);	
});


router.post('/simulator/', function(req, res) {
    let response = {};
    if(req.body.acceptance===true){
        simulations.map((data) => {
            if(data.operationId==req.body.simulationId){
                response.operationId = req.body.simulationId;
                response.client = data.client;
                response.conditions = { capital : data.simulation.capital, 
                                        interes : data.simulation.interes, 
                                        periodo : data.simulation.periodo };
                operations.push(response);
            }
        });
    }else{
        response.error = 'El cliente debe aceptar las condiciones del crédito';
    }
    res.status(200).jsonp(response);	
});





app.use(router);

app.listen(8010, function() {
    console.log("Node server running on http://localhost:8010");
});