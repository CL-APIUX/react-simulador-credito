import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import {Grid, Paper, Typography, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { setClientData, getClientData } from '../../actions/simulatorDataActions'


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(3),
    },
    gridItem: {
        textAlign: 'left',
        flexGrow: 1
    },
    textshort: {
        minWidth: 300,
    },
    textLarge: {
        minWidth: 500,
    }
}));

const ClientData = ({ dispatch, setStepCallBack1, setStepCallBack2 }) => {

    const classes = useStyles();

    const [rut, setRut] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');

    const [saveFlag, setSaveFlag] = useState(true);
    const [rutFlag, setRutFlag] = useState(true);

    const clientRecord= useSelector(state => state.simulatorData.clientData.clientRecord);

    useEffect(()=> {
        if(!!(clientRecord)){
            setRut(clientRecord.rut);
            setFirstName(clientRecord.firstName);
            setLastName(clientRecord.lastName);
            setEmail(clientRecord.email);
            setPhone(clientRecord.phone);
            const objClientData = {
                rut,
                firstName,
                lastName,
                email,
                phone
            }
            dispatch(setClientData(objClientData));
        }
    }, [clientRecord]);

    useEffect(()=> {
        const objClientData = {
            rut,
            firstName,
            lastName,
            email,
            phone
        }
        dispatch(setClientData(objClientData));
    }, [saveFlag]);

    useEffect(()=> {
        if(!!(rut)){
            dispatch(getClientData(rut));
            setStepCallBack2(false);
            setStepCallBack1(true);
        }else{
            setStepCallBack2(true);
            setStepCallBack1(false);
        }
    }, [rutFlag]);


    const handleSave = (rutChange) => {
        setSaveFlag(saveFlag ? false : true);
        if(rutChange){
            setRutFlag(rutFlag ? false : true);
        }
    }

    const handleDescription = (e, callBack) => {
        callBack(e.target.value);
    }

    return (
        <div className={classes.root}>
        <Paper className={classes.paper}>
            <Grid container xs={12} spacing={3}>
                <Grid item xs={12}>
                    <Typography align="left" color="textPrimary" variant="h5">Simula tu crédito de consumo</Typography>
                    <Typography align="left" color="textSecondary" variant="subtitle2">Para comenzar ingresa tu RUT y tus datos de contacto.</Typography>
                    <Typography align="left" color="textSecondary" variant="subtitle2">Todos los campos son obligatorios</Typography>
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                    <TextField 
                        className={classes.textField}
                        label="RUT" 
                        color="secondary" 
                        placeholder = "Ingrese su RUT sin puntos y con guión"
                        value={rut}
                        onChange={e => handleDescription(e, setRut)}
                        onBlur={e => handleSave(true)}
                        variant="outlined" />
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                    <TextField 
                        className={classes.textLarge}
                        label="Nombre" 
                        color="secondary" 
                        placeholder = "Ingrese sus nombres"
                        value={firstName}
                        onChange={e => handleDescription(e, setFirstName)}
                        onBlur={e => handleSave(false)}
                        variant="outlined" />
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                    <TextField 
                        className={classes.textLarge}
                        label="Apellidos" 
                        color="secondary" 
                        placeholder = "Ingrese su apellido paterno y materno"
                        value={lastName}
                        onChange={e => handleDescription(e, setLastName)}
                        onBlur={e => handleSave(false)}
                        variant="outlined" />
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                    <TextField 
                        className={classes.textShort}
                        label="e-mail" 
                        color="secondary" 
                        placeholder = "Ingrese su dirección de correo electrónico"
                        value={email}
                        onChange={e => handleDescription(e, setEmail)}
                        onBlur={e => handleSave(false)}
                        variant="outlined" />
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                    <TextField 
                        className={classes.textShort}
                        label="Teléfono" 
                        color="secondary" 
                        placeholder = "Ingrese su número de teléfono"
                        value={phone}
                        onChange={e => handleDescription(e, setPhone)}
                        onBlur={e => handleSave(false)}
                        variant="outlined" />
                </Grid>
            </Grid>            
        </Paper>
      </div>    
    );

}

export default ClientData