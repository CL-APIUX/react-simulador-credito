import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import {Grid, Paper, TableCell, TableRow, Button, TableContainer, Table, TableHead, TableBody, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(3),
    },
    gridItem: {
        textAlign: 'left',
        flexGrow: 1
    },
    button: {
        margin: theme.spacing(1),
        color: '#fff',
        background: '#F42534',
        border: '1px solid rgba(255, 255, 255, 0.9)',
        margin: 10
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,

    }

}));

const FinishSimulation = ({ dispatch, setStepCallBack2 }) => {

    const classes = useStyles();

    const operationRecord= useSelector(state => state.simulatorData.clientData.operationRecord);
    const clientRecord = useSelector(state => state.simulatorData.clientData.clientRecord);

    useEffect(()=> {
        setStepCallBack2(true);
    }, []);

    const handleSimulation = () => {
        //dispatch(getSimulationtData({rut: clientRecord.rut, capital : capital, period: period, cadency: cadency}));
    } 

    const renderFrenchaAmortizationTable = () => {
        const itemsForRender = !!(operationRecord.frenchaAmortizationTable) ? operationRecord.frenchaAmortizationTable.map ( element => {
            return  (
                <TableRow>
                    <TableCell>{element.numeroCuota}</TableCell>    
                    <TableCell>{element.cuota}</TableCell>    
                    <TableCell>{element.interes}</TableCell>    
                    <TableCell>{element.amortizacion}</TableCell>    
                    <TableCell>{element.saldoInsoluto}</TableCell>    
                    <TableCell>{element.deudaExtinguida}</TableCell>    
                </TableRow>                        
            );
        }) : null;
        return itemsForRender;
    }

    return (
        <div className={classes.root}>
        <Paper className={classes.paper}>
            <Grid container xs={12} spacing={3}>
                <Grid item xs={12}>
                    <Button
                        variant="contained"
                        className={classes.button}
                        onClick={handleSimulation}
                    >Solicitar</Button>
                </Grid>
                <Grid item xs={12}>
                    <Typography align="left" color="textPrimary" variant="h5">{clientRecord.firstName} estás a un click de tu crédito</Typography>
                    <Typography align="left" color="textSecondary" variant="subtitle2">Su crédito de {operationRecord.simulation.capital} a una tasa de interés del {operationRecord.simulation.interes}%</Typography>
                    <Typography align="left" color="textSecondary" variant="subtitle2">Pagarás una cuota de {operationRecord.simulation.cuota} por {operationRecord.simulation.periodo} meses </Typography>
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                    <TableContainer component={Paper}>
                        <Table stickyHeader className={classes.table} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>#</TableCell>    
                                    <TableCell>Cuota</TableCell>    
                                    <TableCell>Interés</TableCell>    
                                    <TableCell>Amortización</TableCell>    
                                    <TableCell>Saldo Insoluto</TableCell>    
                                    <TableCell>Deuda Extinguida</TableCell>    
                                </TableRow>                        
                            </TableHead>
                            <TableBody>
                                {renderFrenchaAmortizationTable()}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>            
        </Paper>
      </div>    
    );

}

export default FinishSimulation