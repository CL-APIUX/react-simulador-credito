import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import {Grid, Paper, InputLabel, TextField, Button, Select, FormControl } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import { makeStyles } from '@material-ui/core/styles';
import { setClientData, getSimulationtData } from '../../actions/simulatorDataActions'


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(3),
    },
    gridItem: {
        textAlign: 'left',
        flexGrow: 1
    },
    button: {
        margin: theme.spacing(1),
        color: '#fff',
        background: '#F42534',
        border: '1px solid rgba(255, 255, 255, 0.9)',
        margin: 10
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,

    }

}));

const YourCredit = ({ dispatch, setStepCallBack2, setStepCallBack3 }) => {

    const classes = useStyles();

    const [capital, setCapital] = useState('');
    const [period, setPeriod] = useState('');
    const [cadency, setCadency] = useState('');
    const [monthlyFee, setMonthlyFee] = useState('');
    const [interes, setInteres] = useState('');
    const [totalAmount, setTotalAmount] = useState('');

    const [saveFlag, setSaveFlag] = useState(true);

    const operationRecord= useSelector(state => state.simulatorData.clientData.operationRecord);
    const clientRecord = useSelector(state => state.simulatorData.clientData.clientRecord);

    useEffect(()=> {
        const objClientData = {
            capital,
            period,
            cadency
        }
        dispatch(setClientData(objClientData));
    }, [saveFlag]);

    useEffect(()=> {
        if(!!operationRecord){
            setCapital(operationRecord.simulation.capital);
            setPeriod(operationRecord.simulation.period);
            setMonthlyFee(operationRecord.simulation.cuota);
            setInteres(operationRecord.simulation.interes);
            setTotalAmount(operationRecord.simulation.costoTotal);
        }
    }, [operationRecord]);


    const handleSave = () => {
        setSaveFlag(saveFlag ? false : true);
    }
    const handleDescription = (e, callBack) => {
        callBack(e.target.value);
    }

    const handleSelectors = (e, callBack) => {
        callBack(e.target.value);
    } 

    const handleSimulation = () => {
        dispatch(getSimulationtData({rut: clientRecord.rut, capital : capital, period: period, cadency: cadency}));
        setStepCallBack3(false);
    } 


    return (
        <div className={classes.root}>
        <Paper className={classes.paper}>
            <Grid container xs={12} spacing={3}>
                <Grid item xs={12}>
                    <Button
                        variant="contained"
                        className={classes.button}
                        onClick={handleSimulation}
                    >Simular</Button>
                </Grid>
                <Grid item xs={6} className={classes.gridItem}>
                    <TextField 
                        type="number"
                        label="Capital" 
                        color="secondary" 
                        placeholder = "Ingrese el monto que quiere solicitar"
                        value={capital}
                        onChange={e => handleDescription(e, setCapital)}
                        onBlur={e => handleSave}
                        variant="outlined" />
                </Grid>
                <Grid item xs={6} className={classes.gridItem}>
                    <FormControl color="secondary"  variant="outlined" className={classes.formControl}>
                        <InputLabel color="secondary"  htmlFor="period-selection">Número de meses</InputLabel>
                        <Select
                            value={period}
                            onChange={e => handleSelectors(e, setPeriod)}
                            inputProps={{
                                name: 'cadency',
                                id: 'period-selection',
                            }}
                            >
                            <option aria-label="None" value="" />
                            <option value={3}>3 cuotas mensuales</option>
                            <option value={6}>6 cuotas mensuales</option>
                            <option value={12}>12 cuotas mensuales</option>
                            <option value={24}>24 cuotas mensuales</option>
                            <option value={48}>48 cuotas mensuales</option>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={6} className={classes.gridItem}>
                    <FormControl color="secondary"  variant="outlined" className={classes.formControl}>
                        <InputLabel color="secondary"  htmlFor="cadency-selection">Cadencia</InputLabel>
                        <Select
                            value={cadency}
                            onChange={e => handleSelectors(e, setCadency)}
                            inputProps={{
                                name: 'cadency',
                                id: 'cadency-selection',
                            }}
                            >
                            <option aria-label="None" value="" />
                            <option value={0}>0 meses</option>
                            <option value={1}>1 mes</option>
                            <option value={2}>2 meses</option>
                            <option value={3}>3 meses</option>
                            <option value={4}>4 meses</option>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={6} className={classes.gridItem}>
                    <TextField 
                        disabled={true}
                        label="Valor de la cuota" 
                        color="secondary" 
                        value={monthlyFee}
                        onChange={e => handleDescription(e, setMonthlyFee)}
                        onBlur={e => handleSave}
                        variant="outlined" />
                </Grid>
                <Grid item xs={6} className={classes.gridItem}>
                    <TextField
                        disabled={true}
                        label="Interes" 
                        color="secondary" 
                        value={interes}
                        onChange={e => handleDescription(e, setInteres)}
                        onBlur={e => handleSave}
                        variant="outlined" />
                </Grid>
                <Grid item xs={6} className={classes.gridItem}>
                    <TextField 
                        disabled={true}
                        label="Total a pagar" 
                        color="secondary" 
                        value={totalAmount}
                        onChange={e => handleDescription(e, setTotalAmount)}
                        onBlur={e => handleSave}
                        variant="outlined" />
                </Grid>
            </Grid>            
        </Paper>
      </div>    
    );

}

export default YourCredit