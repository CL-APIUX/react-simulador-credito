import React, { useState } from 'react';
import {useDispatch, useSelector} from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Tabs, Tab } from '@material-ui/core';
import { TabPanel, TabProps} from './tabComponent/tabpanel';
import ClientData from './tabs/clientData';
import YourCredit from './tabs/yourCredit';
import FinishSimulation from './tabs/finish';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    tabs : {
        backgroundColor : '#fff',
        color : '#F42534'
    }
}));

const CreditAssistant = () => {

    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [step1, setStep1] = useState(false);
    const [step2, setStep2] = useState(true);
    const [step3, setStep3] = useState(true);

    const dispatch = useDispatch();


    const handleTabChange = (event, newValue) => {
        setValue(newValue);
    }

    return (
        <div className={classes.root}>
        <AppBar position="static">
            <Tabs value={value} onChange={handleTabChange} className={classes.tabs} >
                <Tab disabled={step1} label="Tus datos"  {...TabProps(0)} />
                <Tab disabled={step2} label="Simula tu credito"  {...TabProps(1)} />
                <Tab disabled={step3} label="Solicitalo on-line" {...TabProps(2)}/>
            </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
            <ClientData dispatch={dispatch} setStepCallBack1={setStep1} setStepCallBack2={setStep2} />
        </TabPanel>
        <TabPanel value={value} index={1}>
            <YourCredit dispatch={dispatch} setStepCallBack2={setStep2} setStepCallBack3={setStep3} />
        </TabPanel>
        <TabPanel value={value} index={2}>
            <FinishSimulation dispatch={dispatch} setStepCallBack2={setStep2} />
        </TabPanel>
        </div>
    )
    
}

export default CreditAssistant