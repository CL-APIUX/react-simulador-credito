import React from 'react'
import CreditSimulator from '../containers/creditSimulator'
import { Router, Route, Switch } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import styled from '@emotion/styled'

const Container = styled.div`
  text-align: center;
`
export const history = createBrowserHistory()

function Routes() {
  return (
    <Router history={history}>
      <Container>
        <Switch>
          <Route path="/simulator" component={CreditSimulator} />
        </Switch>
      </Container>
    </Router>
  )
}

export default Routes
