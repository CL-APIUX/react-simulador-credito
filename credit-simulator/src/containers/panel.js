import React , { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import simulatorBG from '../assets/bg-simulador-1.jpg';

const useStyles = makeStyles({
root: {
    marginBottom: '30px',
    marginRight: '15px'
},

media: {
    marginTop: '15px',
    margin: "-70px auto 0",
    width: "90%",
    height: 240,
    borderRadius: "4px",
    boxShadow: "0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23)",
    position: "relative",
    zIndex: 1000
  }
});

export default function Panel() {
  const classes = useStyles();

  return (
    <Grid >
    <Grid container spacing={2} >
        <Grid item xs={12}>
            <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                component="img"
                className={classes.media}
                image={simulatorBG}
                />
                <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    Simulador de crédito de consumo
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                    La simulación de crédito de consumo que da cuenta esta hoja, requiere que el consumidor contratante disponga de un patrimonio y/o ingresos futuros suficientes para pagar su costo total y el valor cuota mensual, durante todo el período del crédito. Esta es sólo una simulación NO vinculante y el otorgamiento del crédito está sujeto a aprobación comercial previa, de acuerdo a las políticas de créditos vigentes al momento de la solicitud. El deudor podrá prepagar este crédito. Hay que considerar que en estos casos el socio deberá pagar una comisión sobre el capital que se prepaga asociada a esta operación, equivalente a 30 días de intereses calculado.
                    Simulación válida sólo durante el día de su emisión, expirando en 24 horas. Los valores expresados en la presente simulación son referenciales y podrían variar al momento del curse.                </Typography>
                </CardContent>
            </CardActionArea>
            </Card>
        </Grid>
    </Grid>
</Grid>

  );
}
