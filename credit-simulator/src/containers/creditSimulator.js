import React from 'react';
import { connect } from 'react-redux';
import {Grid} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CreditAssistant from '../components/creditAssistant'
import Header from './header.js'
import Panel from './panel'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    }
}));

const CreditSimulator = () => {

    return (
        <div className={useStyles.root}>
            <Grid>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Header/>
                    </Grid>
                    <Grid item xs={8}>
                        <CreditAssistant />
                    </Grid>
                    <Grid item xs={4}>
                        <Panel/>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

export default connect()(CreditSimulator);

