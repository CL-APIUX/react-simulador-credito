import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Button, Grid } from '@material-ui/core';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CoopeuchLogo from '../assets/logo-coop-2018.png';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    barStyle: {
      backgroundColor: '#F42534',
      color: '#fff'
    },
    button_ingresar: {
        color: '#fff',
        border: '1px solid rgba(255, 255, 255, 0.9)',
        margin: 10
    },
    button_pago: {
        color: '#F42534',
        background : '#fff',
        border: '1px solid rgba(255, 255, 255, 0.9)'
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    head: {
        margin: 10,
        flexGrow: 1
    },
}));


const Header = () => {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container className={classes.head} spacing={2}>
                <Grid item xs={12} />
            </Grid>
            <AppBar position="static" className={classes.barStyle} >
                <Toolbar>
                <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                    <img src={CoopeuchLogo} />
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                    
                </Typography>
                <Grid>
                    <Button variant="outlined" className={classes.button_pago} >Pago Online</Button>
                    <Button variant="outlined" className={classes.button_ingresar} >Ingresar</Button>
                </Grid>
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default Header;


