import {
    SET_CLIENT_DATA,
    GET_CLIENT_DATA,
    GET_SIMULATION_RECORD
} from '../constants/actionTypes';

const setClientData = (data) => ({
    type: SET_CLIENT_DATA,
    clientData: data,
});

const getClientData = (rut) => ({
    type: GET_CLIENT_DATA,
    rut: rut
})

const getSimulationtData = (params) => ({
    type: GET_SIMULATION_RECORD,
    params: params
})

export {
    setClientData,
    getClientData,
    getSimulationtData
};
