import { createStore, applyMiddleware, combineReducers  } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas';
import simulatorData from '../reducers/simulatorReducer'

const rootReducer = combineReducers({
    simulatorData
})

const sagaMiddleware = createSagaMiddleware();

const logger = store => next => action => {
    console.log('CTL: dispatching', action)
    let result = next(action)
    console.log('CTL: next state', store.getState())
    return result
}

const store = createStore(
    rootReducer,
    applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);

export default store;