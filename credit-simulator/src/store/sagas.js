import { call, put, takeLatest, takeEvery, all } from 'redux-saga/effects';
import axios from 'axios';

function getClientDataApi(action){
    return axios.get('http://localhost:8010/simulator/client/' + action.rut, null, null);
}

function getSimultationApi(action){
    console.log('CTL: getSimultationApi', action)
    const parameters = {
        rut: action.params.rut,
        capital: action.params.capital,
        periodo: action.params.period,
        cadencia : action.params.cadency
    }
    return axios.post('http://localhost:8010/simulator/simulate/', parameters, null);
}

function getClientDataSucess(clientRecord){
    return {
        type: 'SET_CLIENT_RECORD',
        clientRecord
    };
}

function getClientDataError(){
    return {
        type: 'SET_CLIENT_RECORD_ERROR'
    };
}

function getSimulationDataSucess(simulationRecord){
    return {
        type: 'SET_SIMULATION_RECORD',
        simulationRecord
    };
}

function getSimulationDataError(){
    return {
        type: 'SET_SIMULATION_RECORD_ERROR'
    };
}

function* clientDataAction(rut) {

    try{
        const response = yield call(getClientDataApi, rut);
        yield put(getClientDataSucess(response.data))
    }catch(error){
        yield put(getClientDataError())
    }
}

export function* clientDataActionWatcher() {
    yield takeEvery('GET_CLIENT_DATA', clientDataAction);
}

function* simulationAction(params) {
    try{
        const response = yield call(getSimultationApi, params);
        yield put(getSimulationDataSucess(response.data))
    }catch(error){
        yield put(getSimulationDataError())
    }
}

export function* simulationActionWatcher() {
    yield takeEvery('GET_SIMULATION_RECORD', simulationAction);
}


export default function* rootSaga() {
    yield all([
        clientDataActionWatcher(),
        simulationActionWatcher()
    ]);
 }


