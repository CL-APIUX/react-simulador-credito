import { combineReducers } from 'redux';

import {
    SET_CLIENT_DATA,
    SET_CLIENT_RECORD,
    SET_CLIENT_RECORD_ERROR,
    SET_SIMULATION_RECORD
} from '../constants/actionTypes';

const setClientDataReducer = (state={}, action) => {
    switch (action.type) {
        case SET_CLIENT_DATA:
            return {
                ...state,
                clientData: action.clientData
            }
        case SET_CLIENT_RECORD:
            return {
                ...state,
                clientRecord: action.clientRecord
            }
        case SET_CLIENT_RECORD_ERROR:
            return {
                ...state,
                clientRecord: {}
            }
        case SET_SIMULATION_RECORD:
            return {
                ...state,
                operationRecord: action.simulationRecord
            }
        default:
            return {
                ...state
            }
    }
}

export default combineReducers({
    clientData : setClientDataReducer
})
